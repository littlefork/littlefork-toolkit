import fs from 'fs-extra';
import {runner, plugins as lfPlugins, queries, data, envelope, utils} from 'littlefork';

import {plugins as csvPlugins} from 'littlefork-plugin-csv';
import {plugins as ddgPlugins} from 'littlefork-plugin-ddg';
import {plugins as guardianPlugins} from 'littlefork-plugin-guardian';
import {plugins as httpPlugins} from 'littlefork-plugin-http';
import {plugins as languageDetectionPlugins} from 'littlefork-plugin-language-detection';
import {plugins as mediaPlugins} from 'littlefork-plugin-media';
import {plugins as mongodbPlugins} from 'littlefork-plugin-mongodb';
import {plugins as searxPlugins} from 'littlefork-plugin-searx';
import {plugins as tapPlugins} from 'littlefork-plugin-tap';
import {plugins as telegramPlugins} from 'littlefork-plugin-telegram';
import {plugins as torPlugins} from 'littlefork-plugin-tor';
import {plugins as twitterPlugins} from 'littlefork-plugin-twitter';
import {plugins as waybackMachinePlugins} from 'littlefork-plugin-wayback-machine';
import {plugins as youtubePlugins} from 'littlefork-plugin-youtube';

// Put the littlefork-cli binary in place
fs.copySync('node_modules/littlefork-cli/bin/littlefork', 'bin/littlefork');

const core = {
  runner, lfPlugins, queries, data, envelope, utils,
};

const plugins = {
  csvPlugins,
  ddgPlugins,
  guardianPlugins,
  httpPlugins,
  languageDetectionPlugins,
  mediaPlugins,
  mongodbPlugins,
  searxPlugins,
  tapPlugins,
  telegramPlugins,
  torPlugins,
  twitterPlugins,
  waybackMachinePlugins,
  youtubePlugins,
};

export {
  runner,
  lfPlugins,
  queries,
  data,
  envelope,
  utils,
  core,
  plugins,
  csvPlugins,
  ddgPlugins,
  guardianPlugins,
  httpPlugins,
  languageDetectionPlugins,
  mediaPlugins,
  mongodbPlugins,
  searxPlugins,
  tapPlugins,
};

export default {
  runner,
  lfPlugins,
  queries,
  data,
  envelope,
  utils,
  core,
  plugins,
  csvPlugins,
  ddgPlugins,
  guardianPlugins,
  httpPlugins,
  languageDetectionPlugins,
  mediaPlugins,
  mongodbPlugins,
  searxPlugins,
  tapPlugins,
};
